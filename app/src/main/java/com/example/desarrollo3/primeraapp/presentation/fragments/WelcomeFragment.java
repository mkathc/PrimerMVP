package com.example.desarrollo3.primeraapp.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.desarrollo3.primeraapp.R;
import com.example.desarrollo3.primeraapp.core.BaseFragment;
import com.example.desarrollo3.primeraapp.data.entities.WelcomeEntity;
import com.example.desarrollo3.primeraapp.presentation.adapters.WelcomeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class WelcomeFragment extends BaseFragment {
    @BindView(R.id.tv_welcome)
    TextView tvWelcome;
    @BindView(R.id.rv_welcome)
    RecyclerView rvWelcome;

    //setea el funcionamiento de los item y  la lista
    WelcomeAdapter welcomeAdapter;
    //indica la posicion
    LinearLayoutManager linearLayoutManager;

    // Singleton
    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_welcome, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // creamos una lista de objetos WelcomeEntity
        ArrayList<WelcomeEntity> list = new ArrayList<>();
        // le agregamos valores a la lista, imagen, titulo y subtitulo
        list.add(new WelcomeEntity(R.drawable.a73c5024a0ada0bbc880ce255470fe1c,"HOLA","primer gato"));
        list.add(new WelcomeEntity(R.drawable.f0f5d4fabbc03e9377a0ebdb475b8746,"HOLA2","segundo gato"));
        list.add(new WelcomeEntity(R.drawable.michi,"HOLA3","tercer gato"));

        // creamos un lineas que nos indicara la posicion, vertical en este caso
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // le mandamos la orientacion al RecyclerView
        rvWelcome.setLayoutManager(linearLayoutManager);
        // creamos un adaptador y le mandamos la lista y el contexto
        welcomeAdapter = new WelcomeAdapter(list, getContext());
        // le pasamos el adaptador al RecyclerView
        rvWelcome.setAdapter(welcomeAdapter);
    }
}
