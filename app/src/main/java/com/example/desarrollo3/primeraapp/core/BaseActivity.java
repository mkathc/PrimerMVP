package com.example.desarrollo3.primeraapp.core;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class BaseActivity extends AppCompatActivity {
    //// ---> Abstraemos el código reutilizable para los Activity


    // Iniciar un fragment en un activity, parámetros: id del frameLayout
    // alojado en la actividad y el newInstance creado en el fragmento que se va a llamar
    public void setFragmentToActivity(int body , Fragment fragment){

        // Creamos un fragmentManager para interactuar con los fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        // iniciamos una transaccion
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        // pasamos los parametros id y el fragment
        fragmentTransaction.add(body, fragment);
        fragmentTransaction.commit();
    }
}
