package com.example.desarrollo3.primeraapp.data.entities;

import com.example.desarrollo3.primeraapp.presentation.fragments.WelcomeFragment;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

//// ---> Modelo de nuestro objeto que será mostrado en el RecyclerView


public class WelcomeEntity {

    String title;
    String subtitle;
    int image;

    public WelcomeEntity(int image, String title, String subtitle) {
        this.image = image;
        this.subtitle = subtitle;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }



}
