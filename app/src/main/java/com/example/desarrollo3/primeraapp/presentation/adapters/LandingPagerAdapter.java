package com.example.desarrollo3.primeraapp.presentation.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class LandingPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> gatos;

    //Adaptador que recibe parametros: FragmenManager y la lista
    public LandingPagerAdapter(FragmentManager fm , ArrayList<Fragment> list) {
        super(fm);
        gatos = list;
    }
    //Obtiene el contenido de la lista
    @Override
    public Fragment getItem(int position) {
        return gatos.get(position);
    }

    //obtiene el tamaño de la lista
    @Override
    public int getCount() {
        return gatos.size();
    }
}
