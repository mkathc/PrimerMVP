package com.example.desarrollo3.primeraapp.presentation.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.desarrollo3.primeraapp.R;
import com.example.desarrollo3.primeraapp.core.BaseActivity;
import com.example.desarrollo3.primeraapp.presentation.fragments.LandingFragment;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class LandingActivity extends BaseActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializar fragment
        setFragmentToActivity(R.id.body, LandingFragment.newInstance());
    }
}
