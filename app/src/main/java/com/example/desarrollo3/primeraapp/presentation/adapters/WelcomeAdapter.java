package com.example.desarrollo3.primeraapp.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.desarrollo3.primeraapp.R;
import com.example.desarrollo3.primeraapp.data.entities.WelcomeEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class WelcomeAdapter extends RecyclerView.Adapter<WelcomeAdapter.ViewHolder> {

    ArrayList<WelcomeEntity> list;
    Context context;

    // constructor que recibe lista y contexto
    public WelcomeAdapter(ArrayList<WelcomeEntity> list, Context context) {
        this.list = list;
        this.context = context;
    }

    // creamos un ViewHolder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_image)
        ImageView ivImage;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_subtitle)
        TextView tvSubtitle;

        // Vista que contiene un item
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflamos la vista
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_welcome_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Creamos un modelo a quien le pasamos el contenido de la lista
        WelcomeEntity welcomeEntity = list.get(position);

        holder.tvTitle.setText(welcomeEntity.getTitle());
        holder.tvSubtitle.setText(welcomeEntity.getSubtitle());
        holder.ivImage.setBackground(context.getResources().getDrawable(welcomeEntity.getImage()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
