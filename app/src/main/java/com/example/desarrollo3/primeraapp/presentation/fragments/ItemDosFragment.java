package com.example.desarrollo3.primeraapp.presentation.fragments;



import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.desarrollo3.primeraapp.R;

/**
 * Created by Desarrollo3 on 1/02/2017.
 */

public class ItemDosFragment extends Fragment {
    //Singleton
    public static ItemDosFragment newInstance(){
        return new ItemDosFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // inflamos la vista, parámetros: nombre del layout, el ViewGroup y false para no anidarlo a la actividad
        // true cierra la app
        View view = inflater.inflate(R.layout.item_viewpager_landing_2, container , false);
        return view;

    }
}
